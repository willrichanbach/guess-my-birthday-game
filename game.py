from random import randint
name = input("What's your name? ")

guess_number = 1
month_number = randint(1, 12)
year_number = randint(1924, 2004)

for guesses in range(1, 6):
    print("Guess ", guesses, ":", name, "Were you born in",
        month_number, "/", year_number, "?")

    response = input("yes or no? ")

    if response == "yes":
            print("I knew it!")
            exit()
    else:
        print("Nope, let me try again!")

print("Welps, I have better things to do")
